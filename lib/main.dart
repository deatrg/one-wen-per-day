import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'page/reader.dart';
import 'common/util.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  debugPaintSizeEnabled = true; //2.第二步
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
      theme: ThemeData.light().copyWith(
        canvasColor: LightCanvasColor,
      ),
      darkTheme: ThemeData.dark().copyWith(
        canvasColor: DarkCanvasColor,
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key?key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    setDarkMode(MediaQuery.of(context).platformBrightness == Brightness.dark);
    return const Material(
      child: ReaderPage(),
    );
  }
}
