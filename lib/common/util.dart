import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

const LightCanvasColor = Color(0xfff8f8fa);
const DarkCanvasColor = Color(0xff373737);

setDarkMode(bool isDark) {
  var color = isDark ? DarkCanvasColor : LightCanvasColor;
  var brightness = isDark ? Brightness.light : Brightness.dark;
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: color,
    statusBarIconBrightness: brightness,
    systemNavigationBarColor: color,
    systemNavigationBarIconBrightness: brightness,
  ));
}