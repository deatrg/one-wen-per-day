import 'package:flutter/material.dart';
import '../models/article.dart';

typedef ReadFavoriteArticleCallback = Function(int index);

class DrawerWidget extends StatelessWidget {
  const DrawerWidget(
      {Key? key,
      required this.articleModel,
      required this.readFavoriteArticleCallback})
      : super(key: key);

  final ArticleModel articleModel;
  final ReadFavoriteArticleCallback readFavoriteArticleCallback;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
              height: 48,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text.rich(
                  TextSpan(
                    children: <InlineSpan>[
                      WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child: Icon(
                          Icons.playlist_add_check_sharp,
                          size: 24,
                        ),
                      ),
                      TextSpan(text: '收藏的文章'),
                    ],
                  ),
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 22),
                ),
              )),
          Expanded(
            child: articleModel.favoriteArticlesList.isEmpty
                ? const Center(
                    child: Text(
                      "暂无收藏的文章",
                      style: TextStyle(fontSize: 18),
                    ),
                  )
                : ListView.builder(
                    itemExtent: 56,
                    itemCount: articleModel.favoriteArticlesList.length,
                    itemBuilder: (context, index) {
                      var titleAuthor =
                          articleModel.favoriteArticlesList[index].split("_");
                      var title = titleAuthor[0];
                      var author = titleAuthor[1];
                      return SizedBox.expand(
                        child: TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                            readFavoriteArticleCallback(index);
                          },
                          child: SizedBox.expand(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  title,
                                  style: const TextStyle(fontSize: 16),
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: false,
                                ),
                                Text(
                                  author,
                                  style: const TextStyle(fontSize: 12),
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: false,
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
          ),
        ],
      ),
    );
  }
}
