import 'package:flutter/material.dart';
import '../models/article.dart';

class ArticleWidget extends StatelessWidget {
  const ArticleWidget(
      {Key? key,
        required this.article,
        required this.contentFontSize,
        required this.lineSpacingTimes,
        required this.scrollController})
      : super(key: key);

  final Article article;
  final double contentFontSize;
  final double lineSpacingTimes;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListView(
        padding: const EdgeInsets.all(10),
        controller: scrollController,
        children: [
          Text(
            article.title,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 26),
          ),
          Text(
            article.author,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 18),
          ),
          Text(
            article.content,
            style: TextStyle(
              fontSize: contentFontSize,
              height: lineSpacingTimes,
            ),
          ),
        ],
      ),
    );
  }
}