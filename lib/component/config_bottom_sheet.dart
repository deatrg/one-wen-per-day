import 'package:flutter/material.dart';
import '../models/article.dart';

typedef UpdateConfigCallback = Function(
    double contentFontSize, double lineSpacingTimes);

class ConfigBottomSheet extends StatefulWidget {
  const ConfigBottomSheet({
    Key? key,
    required this.articleModel,
    required this.updateConfigCallback,
    required this.refreshArticleCallback,
  }) : super(key: key);

  final ArticleModel articleModel;
  final UpdateConfigCallback updateConfigCallback;
  final VoidCallback refreshArticleCallback;

  @override
  State<StatefulWidget> createState() => ConfigBottomSheetState();
}

class ConfigBottomSheetState extends State<ConfigBottomSheet> {
  late StateSetter favoriteState;

  void _changeCurArticleFavoriteState() {
    var articleModel = widget.articleModel;
    if (articleModel.isCurArticleFavorite()) {
      articleModel.setCurArticleUnFavorite();
    } else {
      articleModel.setCurArticleFavorite();
    }
  }

  @override
  Widget build(BuildContext context) {
    double contentFontSize = widget.articleModel.getContentFontSize();
    double lineSpacingTimes = widget.articleModel.getLineSpacingTimes();
    final Color prColor = Theme.of(context).primaryColor;

    return SizedBox(
      height: 160,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flex(
            direction: Axis.horizontal,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 32,
                child: Icon(
                  Icons.format_size_rounded,
                  color: prColor,
                ),
                alignment: Alignment.centerRight,
              ),
              Expanded(
                child: StatefulBuilder(
                  builder: (context, setSliderState) {
                    return Slider(
                      max: 26,
                      min: 14,
                      divisions: 12,
                      label: "${contentFontSize.toInt()}pt",
                      value: contentFontSize,
                      onChangeEnd: (value) =>
                          widget.articleModel.updateContentFontSize(value),
                      onChanged: (value) {
                        setSliderState(() {
                          contentFontSize = value;
                          widget.updateConfigCallback(
                              contentFontSize, lineSpacingTimes);
                        });
                      },
                    );
                  },
                ),
              ),
            ],
          ),
          Flex(
            direction: Axis.horizontal,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 32,
                child: Icon(
                  Icons.format_line_spacing_rounded,
                  color: prColor,
                ),
                alignment: Alignment.centerRight,
              ),
              Expanded(
                child: StatefulBuilder(
                  builder: (context, setSliderState) {
                    return Slider(
                      max: 3,
                      min: 1,
                      divisions: 20,
                      label: "$lineSpacingTimes倍行距",
                      value: lineSpacingTimes,
                      onChangeEnd: (value) =>
                          widget.articleModel.updateLineSpacingTimes(value),
                      onChanged: (value) {
                        setSliderState(() {
                          lineSpacingTimes = value;
                          widget.updateConfigCallback(
                              contentFontSize, lineSpacingTimes);
                        });
                      },
                    );
                  },
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  widget.refreshArticleCallback();
                },
                child: Row(
                  children: const [
                    Icon(Icons.refresh_rounded),
                    Text("刷新文章"),
                  ],
                ),
              ),
              StatefulBuilder(
                builder: (context, setFavoriteState) {
                  return TextButton(
                    onPressed: () {
                      setFavoriteState(() {});
                      _changeCurArticleFavoriteState();
                    },
                    child: Row(
                      children: [
                        widget.articleModel.isCurArticleFavorite()
                            ? const Icon(Icons.favorite_rounded)
                            : const Icon(Icons.favorite_border_rounded),
                        const Text("收藏文章"),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
